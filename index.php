<?php

// require("animal.php");
require("frog.php");
require("ape.php");

$sheep = new animal("shaun");

echo "Nama: " . $sheep->name . "<br>"; // "shaun"
echo "Jumlah Kaki: " . $sheep->legs . "<br>"; // 4
echo "Berdarah dingin? " . $sheep->cold_blooded . "<br><br>"; // "no"

$animal2 = new Frog("buduk");

echo "Nama: " . $animal2->name . "<br>";
echo "Jumlah Kaki: " . $animal2->legs . "<br>";
echo "Berdarah dingin? " . $animal2->cold_blooded . "<br>";
$animal2->jump();
echo "<br><br>";

$animal3 = new Ape("kera sakti");

echo "Nama: " . $animal3->name . "<br>";
echo "Jumlah Kaki: " . $animal3->legs . "<br>";
echo "Berdarah dingin? " . $animal3->cold_blooded . "<br>";
$animal3->yell();
echo "<br><br>";
